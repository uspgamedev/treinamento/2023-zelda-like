extends CharacterBody2D

@onready var animation_player = $AnimationPlayer

enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN,
	NONE
}

var last_facing: Direction
var _going_to: Direction
var timer: float = 0.0
@export var move_time := 1.0
@export var speed = 100.0
@export var health := 10
@onready var body_scale = scale

func _physics_process(delta: float) -> void:
	match _going_to:
		Direction.LEFT:
			position.x += -speed * delta
			animation_player.play("walk_horizontal")
			scale.x = body_scale.x
		Direction.RIGHT:
			position.x += speed * delta
			animation_player.play("walk_horizontal")
			scale.x = -body_scale.x
		Direction.UP:
			position.y += -speed * delta
			animation_player.play("walk_up")
		Direction.DOWN:
			position.y += speed * delta
			animation_player.play("walk_down")
		Direction.NONE:
			match last_facing:
				Direction.LEFT:
					animation_player.play("idle_horizontal")
					scale.x = body_scale.x
				Direction.RIGHT:
					animation_player.play("idle_horizontal")
					scale.x = -body_scale.x
				Direction.UP:
					animation_player.play("idle_up")
				Direction.DOWN:
					animation_player.play("idle_down")
	
	last_facing = _going_to
	
	timer += delta
	if timer > move_time:
		timer = 0
		_going_to = Direction.values()[randi() % Direction.size()]
	
	if health <= 0:
		queue_free()
	
	pass

func take_damage(damage: int):
	health -= damage
	pass
