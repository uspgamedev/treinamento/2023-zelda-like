extends CharacterBody2D



const SPEED = 100.0
const JUMP_VELOCITY = -400.0
var direction
var orientation = Vector2.ZERO;
var openingChest = false
var keysCollected = 0
var dashing = false
var canDash = false
@export var health := 10

enum player_State{idle, atacando, dashing}

var		player_state: player_State = player_State.idle


func _physics_process(_delta: float) -> void:
	if player_state == player_State.idle:
		direction = -(Input.get_vector("ui_left", "ui_right", "ui_down", "ui_up"))

	
	if direction != Vector2.ZERO:
		orientation = direction
	
	
		
	
	if direction:
		$Sword.rotation = direction.angle()
		velocity = (direction * SPEED) * 0.8
	elif direction and dashing == true:
		$Sword.rotation = direction.angle()
		velocity = (direction * SPEED) * 32
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.y = move_toward(-velocity.y, 0, SPEED)
	
	if player_state == player_State.idle:
		move_and_slide()

func _input(event):
	if event.is_action_pressed("ui_accept") and $Sword.emAtaque == false:
		player_state = player_State.atacando
		print("atacou")
		
		await $Sword.ataque(self.orientation)
		player_state = player_State.idle
		print("acabou")
		
	if event.is_action_pressed("dash") and $Sword.emAtaque == false and canDash == true:
		if $Timer.is_stopped():
				$Timer.start(0.3)
				dashing = true
				self.set_collision_mask_value(3, false)
				velocity = (direction * SPEED * 8)
				velocity.x = move_toward(velocity.x, 0, SPEED)
				velocity.y = move_toward(velocity.y, 0, SPEED)
				move_and_slide()
		
		
	
		
		print("dash")


func _on_timer_timeout():
	self.set_collision_mask_value(3, true)
	$Timer.stop()
	dashing = false
	pass # Replace with function body.

func take_damage(damage: int):
	health -= damage
	if health <= 0:
		queue_free()
	pass



