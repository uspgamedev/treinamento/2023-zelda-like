extends Node2D

var emAtaque = false
var knockbackDirection = Vector2(0,0)
# Called when the node enters the scene tree for the first time.
func _ready():
	$Area2D/CollisionShape2D.disabled = true



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	
	
	
	pass

func ataque(orientation : Vector2):

	knockbackDirection = orientation
	self.emAtaque = true
	$Area2D/CollisionShape2D.disabled = false
	var tween = create_tween()
	tween.set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(self, "position", self.position + (orientation * 80), 0.09)
	await tween.finished
	var tween2 = create_tween()
	tween2.tween_property(self, "position", self.position - (orientation * 80), 0.09)
	await tween2.finished
	self.emAtaque = false
	$Area2D/CollisionShape2D.disabled = true

	#tween.tween_callback(func():
	#	var tween2 = create_tween()
	#	tween2.tween_property(self, "position", self.position - Vector2(10,0), 0.5))

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		var tween3 = create_tween()
		var tween2 = create_tween()
		tween3.set_ease(Tween.EASE_IN_OUT)
		tween2.tween_property(body, "modulate:v", 1, 0.15).from(15)
		tween3.tween_property(body, "position", body.position + (knockbackDirection * 25), 0.1)
		await tween3.finished
		await tween2.finished
		body.take_damage(5)

