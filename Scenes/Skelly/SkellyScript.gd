
extends CharacterBody2D

@onready var animation_player = $AnimationPlayer

enum Direction {
	LEFT,
	RIGHT,
	UP,
	DOWN,
	NONE
}

var last_facing: Direction
var _going_to: Direction
var timer: float = 0.0
@export var move_time := 1.0
@export var speed = 10.0
@export var health := 10
@export var damageDeal := 5
@export var pitch_scale : float = 1.0
@onready var body_scale = scale


func _physics_process(delta: float) -> void:
	match _going_to:
		Direction.LEFT:
			$Sprite2D.set_flip_h(false)
			velocity = Vector2(1, 0) * speed
			move_and_slide()
			animation_player.play("walk_horizontal")
			scale.x = body_scale.x
		Direction.RIGHT:
			scale.x = body_scale.x
			$Sprite2D.set_flip_h(true)
			velocity = Vector2(-1, 0) * speed
			move_and_slide()
			animation_player.play("walk_horizontal")
			
		Direction.UP:
			velocity = Vector2(0, -1) * speed
			move_and_slide()
			animation_player.play("walk_up")
		Direction.DOWN:
			velocity = Vector2(0, 1) * speed
			move_and_slide()
			animation_player.play("walk_down")
		Direction.NONE:
			match last_facing:
				Direction.LEFT:
					$Sprite2D.set_flip_h(false)
					animation_player.play("idle_horizontal")
					scale.x = body_scale.x
				Direction.RIGHT:
					$Sprite2D.set_flip_h(true)
					animation_player.play("idle_horizontal")
					scale.x = body_scale.x
				Direction.UP:
					animation_player.play("idle_up")
				Direction.DOWN:
					animation_player.play("idle_down")
	
	last_facing = _going_to
	
	timer += delta
	if timer > move_time:
		timer = 0
		_going_to = Direction.values()[randi() % Direction.size()]
	
	
	
	pass

func take_damage(damage: int):
	$AudioStreamPlayer.set_pitch_scale(pitch_scale)
	$AudioStreamPlayer.play()
	health -= damage
	if health <= 0:
		queue_free()
	pass


func _on_hitbox_body_entered(body):
	if body.is_in_group("Player"):
		print("boom")
		var tween3 = create_tween()
		var tween2 = create_tween()
		tween3.set_ease(Tween.EASE_IN_OUT)
		tween2.tween_property(body, "modulate:v", 1, 0.15).from(15)
		tween3.tween_property(body, "position", body.position + (-body.orientation * 25), 0.1)
		await tween3.finished
		await tween2.finished
		body.take_damage(damageDeal)
