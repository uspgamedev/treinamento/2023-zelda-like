extends Sprite2D

@onready var animation_player = $AnimationPlayer
var activate = false

# Called when the node enters the scene tree for the first time.
func _ready():

	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if activate == true:
		self.visible = true
		animation_player.play("particle")
		await $AnimationPlayer.animation_finished
		self.position = Vector2(300, 300)
		self.visible = false
	pass
