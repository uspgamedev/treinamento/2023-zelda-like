extends CharacterBody2D


signal changed_room
@onready var animation_player = $AnimationPlayer
@onready var dust = get_tree().get_first_node_in_group("Dust")
var SPEED = 100.0
const JUMP_VELOCITY = -400.0
var direction
var orientation = Vector2.ZERO;
var openingChest = false
var keysCollected = 0
var dashing = false
var canDash = false
var end = false


@export var health := 5

enum player_State{idle, atacando, dashing}

var		player_state: player_State = player_State.idle

func _ready():
	orientation = Vector2(0,-1)
	
	$Sword.rotation = Vector2(0,-1).angle()
	move_and_slide()



func _physics_process(_delta: float) -> void:
	if player_state == player_State.idle and end==false:
		direction = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")

	
	if direction != Vector2.ZERO:
		orientation = direction
	

	if player_state == player_State.idle:
		if velocity.x > 0:
			animation_player.play("move_right")
		elif velocity.x < 0:
			animation_player.play("move_left")

		if velocity.y > 0:
			animation_player.play("move_down")
		elif velocity.y < 0:
			animation_player.play("move_up")
		
	if velocity.x == 0 and velocity.y == 0 and player_state == player_State.idle:
		if orientation == Vector2(0,1):
			animation_player.play("idle_down")
		elif orientation == Vector2(0,-1):
			animation_player.play("idle_up")
		elif orientation == Vector2(-1,0):
			animation_player.play("idle_left")
		elif orientation == Vector2(1,0):
			animation_player.play("idle_right")

	
	if direction:
		$Sword.rotation = direction.angle()
		velocity = direction * SPEED
	elif direction and dashing == true:
		$Sword.rotation = direction.angle()
		velocity = (direction * SPEED) * 32
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.y = move_toward(velocity.y, 0, SPEED)
	
	if player_state == player_State.idle:
		move_and_slide()

func _on_screen_exited() -> void:
	emit_signal("changed_room", self)
	pass

func _input(event):
	if event.is_action_pressed("ui_accept") and $Sword.emAtaque == false:
		player_state = player_State.atacando
		$AudioStreamPlayer.play()
		if orientation == Vector2(0,1):
			animation_player.play("attack_down")
		elif orientation == Vector2(0,-1):
			animation_player.play("attack_up")
		elif orientation == Vector2(-1,0):
			animation_player.play("attack_left")
		elif orientation == Vector2(1,0):
			animation_player.play("attack_right")
		
		await $Sword.ataque(self.orientation)
		player_state = player_State.idle

		
	if event.is_action_pressed("dash") and $Sword.emAtaque == false and canDash == true:
		if $Timer.is_stopped():
				$Timer.start(0.3)
				dashing = true
				$AudioStreamPlayer2.play()
				self.set_collision_mask_value(3, false)
				dust.position = self.position
				dust.activate = true
				velocity = (direction * SPEED * 4)
				move_and_slide()
				await $Timer.timeout
		
		
	
		
		print("dash")


func _on_timer_timeout():
	self.set_collision_mask_value(3, true)
	$Timer.stop()
	dashing = false
	pass # Replace with function body.
	
func take_damage(damage: int):
	health -= damage
	$AudioStreamPlayer3.play()
	print(health)
	if health <= 0:
		get_tree().reload_current_scene()
		pass
	pass
