extends Node2D

@export var Player: NodePath
@export var directionA := directions.East

enum directions{North, South, East, West}
const pushDirection = {
	directions.North: Vector2(0,-1),
	directions.South: Vector2(0,1),
	directions.East: Vector2(1,0),
	directions.West: Vector2(-1,0),
}

var openBody = null
var canOpen = false
var opened = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	
	if Input.is_action_pressed("ui_accept") and canOpen and get_node(Player).keysCollected >= 1 and get_node(Player).orientation == pushDirection[directionA]:
		print("abriu")
		canOpen = false
		opened = true
		$AudioStreamPlayer.play()
		self.visible = false
		get_node(Player).keysCollected = get_node(Player).keysCollected - 1
		$StaticBody2D/CollisionShape2D.disabled = true
	pass

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		print("entrou")
		openBody = body
		if opened == false:
			canOpen = true


func _on_area_2d_body_exited(body):
		if body.is_in_group("Player"):
			canOpen = false
