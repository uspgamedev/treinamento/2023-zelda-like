extends Node2D

@export var Contents: NodePath

var openBody = null
var canOpen = false
var opened = false
signal coletou()
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):

	
	if Input.is_action_pressed("ui_accept") and canOpen == true and openBody.orientation == Vector2(0, -1):
		canOpen = false
		opened = true
		self.visible = false
		get_node(Contents).position = self.position + Vector2(0, -8)
		
		if get_node(Contents).has_method("coletar"):
			get_node(Contents).coletar(openBody)
		openBody.openingChest = false
		queue_free()
	pass

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		openBody = body
		openBody.openingChest = true
		if opened == false:
			canOpen = true


func _on_area_2d_body_exited(body):
		if body.is_in_group("Player"):
			canOpen = false
