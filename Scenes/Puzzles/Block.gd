extends Sprite2D

@export var directionA := directions.East

enum directions{North, South, East, West}
const pushDirection = {
	directions.North: Vector2(0,-1),
	directions.South: Vector2(0,1),
	directions.East: Vector2(1,0),
	directions.West: Vector2(-1,0),
}
var pushBody = null
var pushing = false
var pushed = false
signal moveu()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):

	if pushBody != null:
		if pushBody.direction == pushDirection[directionA]:
			if $Timer.is_stopped():
				$Timer.start(0.7)
			pushing = true
		else:
			$Timer.stop()
			pushing = false
	else:
		$Timer.stop()	

	pass

func _on_area_2d_body_entered(body):
	if body.is_in_group("Player"):
		pushBody = body


func _on_timer_timeout():
	if pushed == false:
		print("moveu")
		$Area2D.visible = false
		var tween = create_tween()
		tween.set_ease(Tween.EASE_IN_OUT)
		tween.tween_property(self, "position", self.position + (pushDirection[directionA] * 16), 1)
		$AudioStreamPlayer.play()
		await tween.finished
		moveu.emit()
		pushed = true

	pass # Replace with function body.


func _on_area_2d_body_exited(_body):
		pushBody = null
		pass # Replace with function body.
