extends Node2D

@onready var music = get_tree().get_first_node_in_group("Music")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func coletar(openBody):
	music.playing = false
	openBody.get_child(2).visible = false
	$AudioStreamPlayer.play()
	var tween = create_tween()
	tween.set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(self, "position", self.position + Vector2(0, -8), 1)
	await tween.finished
	openBody.get_child(2).visible = true
	self.position = Vector2(200, 300)
	openBody.keysCollected+=1
	print(openBody.keysCollected)
	music.playing = true
	
	
