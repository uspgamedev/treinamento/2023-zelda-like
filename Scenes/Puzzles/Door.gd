extends Node2D
@export var blockPath: NodePath
var block

# Called when the node enters the scene tree for the first time.
func _ready():
	block = get_node(blockPath)
	block.moveu.connect(abrirPorta)
	pass # Replace with function body.



func abrirPorta():
	$AudioStreamPlayer.play()
	self.visible = false
	$StaticBody2D/CollisionShape2D.disabled = true
	pass
