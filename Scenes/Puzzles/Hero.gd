extends Node2D

@onready var music = get_tree().get_first_node_in_group("Music")
@onready var credits = get_tree().get_first_node_in_group("Credits")
@onready var player = get_tree().get_first_node_in_group("Player")
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func coletar(openBody):
	player.SPEED = 0
	player.direction = Vector2.ZERO
	player.orientation = Vector2(0,-1)
	player.end = true
	music.playing = false
	openBody.get_child(2).visible = false
	$AudioStreamPlayer.play()
	var tween = create_tween()

	tween.set_ease(Tween.EASE_IN_OUT)
	tween.tween_property(self, "position", self.position + Vector2(0, -8), 1)
	await tween.finished
	var tween2 = create_tween()
	tween2.set_ease(Tween.EASE_IN_OUT)
	tween2.tween_property(credits, "position", credits.position + Vector2(0, 56), 7)
	await tween2.finished
	openBody.get_child(2).visible = true


	
	
