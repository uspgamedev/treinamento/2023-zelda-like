extends Node2D

var transitioning: bool = false

@export var duration = 1.0
var camera: Camera2D

func _ready() -> void:
	camera = get_viewport().get_camera_2d()
	pass

func transition_camera(player: CharacterBody2D, duration_override: float = duration) -> void:
	if transitioning: return
	
	transitioning = true
	
	player.set_process(false)
	player.set_physics_process(false)
	
	var tween: Tween = create_tween()
	
	tween.set_parallel(true).set_ease(Tween.EASE_OUT)
	
	var new_position = camera.position
	var viewport = get_viewport_rect()
	
	# RIGHT
	if player.position.x > camera.position.x + viewport.size.x / 2:
		new_position.x += viewport.size.x
	# LEFT
	if player.position.x < camera.position.x - viewport.size.x / 2:
		new_position.x -= viewport.size.x
	# DOWN
	if player.position.y > camera.position.y + viewport.size.y / 2:
		new_position.y += viewport.size.y
	# UP
	if player.position.y < camera.position.y - viewport.size.y / 2:
		new_position.y -= viewport.size.y
	
	tween.tween_property(camera, "position", new_position, duration_override)
	$AudioStreamPlayer2.play()
	await tween.finished
	
	player.set_process(true)
	player.set_physics_process(true)
	
	transitioning = false


func _on_test_player_changed_room(player: CharacterBody2D) -> void:
	transition_camera(player)
	pass # Replace with function body.
