# 2023 Zelda-like

## Descrição
Projeto de treinamento para o primeiro semestre de 2023 do grupo de exetnsão USPGameDev tutorado por Luciano. 
O projeto é um top-down dungeon crawler inspirado no primeiro Legend of Zelda desenvolvido para o NES em 1986. 

## Créditos
Tileset: https://sironionknight.itch.io/dungeon-starter

Sprite do Esqueleto: https://snoblin.itch.io/pixel-rpg-skeleton-free

Fonte: https://www.fontsquirrel.com/fonts/Silkscreen
